Import-Module DnsClient-PS

# Returns true if a conflicting record is found in DNS.

function handler {
    Param($context, $inputs)

    $hostname = $inputs.hostname
    $domain = $inputs.domain
    $fqdn = $hostname + '.' + $domain
    Write-Host "Querying DNS for $fqdn..."
    $resolution = (Resolve-Dns $fqdn)
    If (-not $resolution.HasError) {
        Write-Host "Record found:" ($resolution | Select-Object -Expand Answers).ToString()
        $queryresult = "true"

    } Else {
        Write-Host "No record found."
        $queryresult = "false"
    }
    return $queryresult
}